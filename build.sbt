name := "ScalaCassandraConnector"

organization := "com.benjarman"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "com.datastax.cassandra" % "cassandra-driver-core" % "2.1.5"

libraryDependencies += "joda-time" % "joda-time" % "2.7"
