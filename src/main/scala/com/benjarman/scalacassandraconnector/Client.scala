package com.benjarman.scalacassandraconnector

import com.datastax.driver.core.{PreparedStatement, BoundStatement, Cluster}
import org.joda.time.DateTime

import scala.collection.JavaConverters._

/**
 * Created by benjarman on 3/10/16.
 */
class Client(host: String = "localhost", port: Int = 9042, keySpace: String = "test") {
  private val cluster = Cluster.builder().addContactPoint(host).withPort(port).build()
  private val session = cluster.connect(keySpace)

  private[this] def getFieldTuples(cc: Any): Seq[(String, AnyRef)] = {
    val clas = cc.getClass
    val constructor = clas.getDeclaredConstructors.head
    val argumentCount = constructor.getParameterTypes.size
    val fields = clas.getDeclaredFields
    (0 until argumentCount) map { i =>
      val fieldName = fields(i).getName
      val fieldValue = clas.getMethod(fieldName).invoke(cc)
      (fieldName, fieldValue)
    }
  }

  /*
  We could reduce complexity by having a single fold left that computes the whole thing
   */
  private[this] def createSqlStatement(fields: Seq[(String, AnyRef)], columnFamily: String): String = {
    val head = fields.head
    val tail = fields.tail
    val part0 = s"""INSERT INTO "${columnFamily}" ( "${head._1}""""
    val part1 = tail.foldLeft(part0)( (statement, next) => {
      statement + s""", "${next._1}""""
    })
    val part2 = part1 + " ) VALUES( ?"
    val part3 = tail.foldLeft(part2)( (statement, next) => {
      statement + ",?"
    } )
    val part4 = part3 + " )"
    part4
  }

  /*
  This method does not support custom cassandra types yet
   */
  private[this] def prepareBoundStatement(
                                           statement: PreparedStatement,
                                           fields: Seq[(String, AnyRef)]
                                           ): BoundStatement = {
    val boundStatement = new BoundStatement(statement)
    val count = fields.size
    (0 until count) foreach {i =>
      val fieldValue = fields(i)._2
      prepareField(boundStatement, fieldValue, i)
    }
    boundStatement
  }

  /*
  There are holes in this logic around map and list
  TODO Add support for nested case classes.
   */
  private[this] def prepareField(
                                  boundStatement: BoundStatement,
                                  value: Any,
                                  i: Int
                                  ) {
    value match {
      case null => {
        boundStatement.setToNull(i)
      }
      case None => {
        boundStatement.setToNull(i)
      }
      case s: String => {boundStatement.setString(i, s)}
      case int: Int => {boundStatement.setInt(i, int:java.lang.Integer)}
      case l: Long => {boundStatement.setLong(i, l:java.lang.Long)}
      case dt: DateTime => {
        boundStatement.setDate(i, dt.toDate())
      }
      case tf: Boolean => {boundStatement.setBool(i, tf:java.lang.Boolean)}
      case d: Double => {boundStatement.setDouble(i, d:java.lang.Double)}
      case m: Map[String,String] => {
        if(m.isEmpty){
          boundStatement.setToNull(i)
        }else {
          val fixed = m.map(k => {
            (k._1, Option(k._2).getOrElse("") )
          })
          boundStatement.setMap[String, String](i, fixed.asJava)
        }
      }
      case a: List[String] => {boundStatement.setList(i, a.asJava)}
      case set: Set[String] => {boundStatement.setSet(i, set.asJava)}
      case o: Option[_] => {
        if(o.isDefined){
          prepareField(boundStatement, o.get, i)
        }else{
          if(!o.isInstanceOf[Option[String]]){
            boundStatement.setToNull(i)
          }else {
            prepareField(boundStatement, "", i)
          }
        }
      }
      case default => {
        println(s"Unsupported type!!!! ${default}")
        boundStatement.setToNull(i)
      }
    }

  }

  def saveCaseClass(cc: Any, columnFamily: String): Unit ={
    val fields = getFieldTuples(cc)
    val strStatement = createSqlStatement(fields, columnFamily)
    val statement = session.prepare(strStatement)
    val boundStatement = prepareBoundStatement(statement, fields)
    session.execute(boundStatement)
  }

}
